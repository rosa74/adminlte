<?php

require 'header.php';

$nom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom']: "";
$telephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone']: "";
$email = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email']: "";

if (isset($_POST['submit'])){
if (isset($_POST['nom']) && !empty($_POST['nom'])
&& isset($_POST['prenom']) && !empty($_POST['prenom'])
&& isset($_POST['telephone']) && !empty($_POST['telephone'])
&& isset($_POST['email']) && !empty($_POST['email'])
) {

$req = $pdo->prepare("INSERT INTO etudiant (nom, prenom, telephone, email) VALUES (?,?,?,?)");
$req->execute([$nom, $prenom, $telephone, $email]);
header("Location: listeEtudiant.php");

}
}

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Ajout d'un étudiant</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
                        <li class="breadcrumb-item active">Ajout d'un étudiant</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="post">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nom">Nom</label>
                                    <input type="text" name="nom" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="prenom">Prénom</label>
                                    <input type="text" name="prenom" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="telephoneEtudiant">Téléphone</label>
                                    <input type="tel" name="telephone" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" name="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->

    </section>

    </html>
    <!-- /.content -->
</div>


<?php require 'footer.php' ?>