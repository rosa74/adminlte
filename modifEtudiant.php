<?php

require "connexion.php";
require 'header.php';

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom'] : "";
$recupPrenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom'] : "";
$recupTelephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone'] : "";
$recupEmail = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : "";

$recupIdEtudiant = isset($_GET["id"]) ? $_GET["id"] : "";

$supIntervention = isset($_GET["sup"]) ? $_GET["sup"] : "";

if ($supIntervention == 'ok') {
  $req = $pdo->prepare("DELETE FROM etudiant WHERE id = ?");
  $req->execute([$recupIdEtudiant]);
  header("Location: listeEtudiant.php");
}

$req = $pdo->prepare("SELECT * FROM etudiant
                         WHERE id = ?
                         ");
$req->execute([$recupIdEtudiant]);
$results = $req->fetchALL();
$etudiant = $results[0];

if (isset($_POST['submit'])) {
  if (
    isset($_POST['nom']) && !empty($_POST['nom'])
    && isset($_POST['prenom']) && !empty($_POST['prenom'])
    && isset($_POST['telephone']) && !empty($_POST['telephone'])
    && isset($_POST['email']) && !empty($_POST['email'])
  ) {

    $req = $pdo->prepare("UPDATE etudiant SET nom=?, prenom=?, telephone=?, email=? WHERE id=?");
    $req->execute([$recupNom, $recupPrenom, $recupTelephone, $recupEmail, $recupIdEtudiant]);
    header("Location: listeEtudiant.php");
  }
}


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Modification de l'étudiant <?php echo $etudiant['prenom']  ." " . $etudiant['nom'] ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Accueil</a></li>
            <li class="breadcrumb-item active">Modification d'un étudiant</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post">
              <div class="card-body">
                <div class="form-group">
                  <label for="nom">Nom</label>
                  <input type="text" name="nom" class="form-control" placeholder="" value="<?php echo $etudiant['nom'] ?>">
                </div>
                <div class="form-group">
                  <label for="prenom">Prénom</label>
                  <input type="text" name="prenom" class="form-control" placeholder="" value="<?php echo $etudiant['prenom'] ?>">
                </div>
                <div class="form-group">
                  <label for="telephone">Téléphone</label>
                  <input type="tel" name="telephone" class="form-control" placeholder="" value="<?php echo $etudiant['telephone'] ?>">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" class="form-control" placeholder="" value="<?php echo $etudiant['email'] ?>">
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" name="submit" class="btn btn-primary">Modifier</button>
              </div>
              <div><a href='listeEtudiant.php?id=<?php echo ($etudiant["id"]) ?> ' style="background-color:#000000; color:white; text-align:center; padding:5px 20px; margin-left:20px; font-size:20px; border-radius:5px;">Retour liste</a></div>

            </form>
          </div>
          <!-- /.card -->


        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div><a href="?sup=ok&id=<?php echo $etudiant["id"] ?>" style="background-color: #d540ed ; color:white; text-align:center; padding:5px 20px; margin-left:20px; font-size:20px; border-radius:5px;">Supprimer cet étudiant</a></div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<?php

require "footer.php";
